# LAYOUTS

**This directory is not required, you can delete it if you don't want to use it.**

This directory contains your Application Layouts.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/guide/views#layouts).


**default.vue is responsible for adding navbar and sidebar in any component which is wrapped with <v-layout></v-layout>**

1. Layout contains the navbar, Sidebar and footer component.
2. navbar and sidebar of layout is being designed by vuetify's app-bar commponent